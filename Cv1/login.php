<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<title>My very first web</title>
</head>

<body>
	<main>
		<!-- Navigace --> 
		<nav>
			<ul>
				<li> <a href="#html">Html</a></li>
				<li> <a href="#">Menu 2</a></li>
				<li> <a href="#">Menu 3</a></li>
				<li> <a href="login.php">Login</a></li>
			</ul>			
		</nav>

		<section id='login_form'>
			<form method="get">
				<label>Username
					<input type="text" name="username" required>
				</label>
				<br>

				<label>Password
					<input type="password" name="passwd" required>
				</label>
				<br>

				<label>Planet
					<select name="planet">
						<option value="Earth">Earth</option>
						<option value="Betelgeuze">Betelgeuze</option>
						<option value="Proxima B">Proxima B</option>
					</select>
				</label>
				<br>
				<input type="submit" value="Send">
			</form>	
		</section>

		<section>
			<?php
				if (!empty($_GET['username'])) {
					print "<h1>Welcome " . htmlspecialchars($_GET['username']) . "</h1>";
					print "<p>visitor from {$_GET['planet']}!</p>";

					$users = file_get_contents("https://akela.mendelu.cz/~xvalovic/mock_users.json");
					$users_json = json_decode($users);
					print "
					<table> 
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Gender</th>
						</tr>";
					foreach ($users_json as $u) {
						print "
							<tr> 
								<td> $u->id </td>
								<td> $u->first_name </td>
								<td> $u->email </td>
								<td> $u->gender </td>
							</tr>
						";
					}
					print "</table>";
				}
			?>
		</section>
	</main>
</body>
</html>

